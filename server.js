const http = require("http");
const express = require("express");
const app = express();

const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);
app.use(express.json({ extended: false }));
app.use(express.static("dist"));

var port = process.env.PORT || 3000;

let players = new Map();

io.on("connection", (socket) => {
  console.log(`computer connected`);
  socket.on("join", (msg) => {
    const { userName, color } = msg;
    players.set(socket.id, { userName, color });
    console.log("send updated playerlist", [...players.entries()]);
    io.sockets.emit("playerlistUpdated", [...players.values()]);
  });

  socket.on("objectMoved", (msg) => {
    //console.log(`player moved ${msg}`);
    socket.broadcast.emit("objectMoved", msg);
  });

  socket.on("disconnect", (reason) => {
    console.log(`player disconected`);
    players.delete(socket.id);
    io.sockets.emit("playerlistUpdated", [...players.values()]);
  });
});

server.listen(port, () => {
  console.log("listening on *:", port);
});
