import io from "socket.io-client";
function connect() {
  var scheme = "ws";
  var location = document.location;

  if (location.protocol === "https:") {
    scheme += "s";
  }

  const serverUrl = `${scheme}://${location.hostname}:${location.port}`;

  const socket = io(serverUrl); //new WebSocket(serverUrl, "json");
  return socket;
}

export default connect;
