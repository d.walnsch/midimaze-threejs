//Midimaze4web
import * as THREE from "three";
import { MeshStandardMaterial } from "three";
//import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import "./style.css";
import connect from "./ws";
const canvas = document.getElementById("webgl")!;

type Player = {
  userName: string;
  smiley: THREE.Mesh<THREE.SphereGeometry, THREE.Material | THREE.Material[]>;
  bullet: THREE.Mesh<THREE.SphereGeometry, THREE.Material | THREE.Material[]>;
};

type PlayerlistUpdated = {
  userName: string;
  color: string;
}[];

type ObjectMoved = {
  userName: string;
  position: [x: number, y: number, z: number];
  rotation: number[];
  type: "smiley" | "bullet";
};

const players = new Map<string, Player>();
const updates: ObjectMoved[] = [];

// SCENE
const scene = new THREE.Scene();
scene.background = new THREE.Color("0xccc");
scene.fog = new THREE.FogExp2("0xccc", 0.1);
// FLOOR

const socket = connect();

const floorGroup = new THREE.Group();
const floorMesh = new THREE.Mesh(
  new THREE.PlaneGeometry(200, 200),
  new THREE.MeshStandardMaterial({ color: "0xccc" })
);
floorMesh.receiveShadow = true;
floorMesh.rotation.set(-Math.PI / 2, 0, 0);

floorGroup.add(floorMesh);
scene.add(floorGroup);

// WALL

// Improvement: mark occupied places and connect centers of two neighbors with wall.
// Google .MAZ Files
const map = `
-------------------------------
|                             |
|    --------------------     |
|    |               |  |     |
|    |    -------    |  |     |
|    |               |  |     |
|    |               |  |     |
|                    ----     |
|                             |
|         --------            |
|    |        |  |       |    |
|    |        |  |       |    |
|    |        |  |       |    |
|             ----            |
|                             |
-------------------------------
`;

/*
New One with Doors
Markus Fritze
24
XXXXXXXXXXXXXXXXXXXXXXXXX
X.......................X
X.XXXXX.XXXXX.XXX.XXXXX.X
X.X...D.D...X...X.....X.X
X.X.XXX.XXX.XXX.XXXXX.X.X
X...X.X.X.X.....X.....X.X
XXX.XXX.XXXXX.XXX.XXX.X.X
X.X...X.....X.X...X.X.X.X
X.X.X.XXX.X.X.X.XXXXX.X.X
X...X.....X...X.X.X...X.X
X.XXXDXXXXXXX.X.XXX.X.X.X
X.X...........X.....X...X
X.X..I.I.IXIX.XXXXX.XXX.X
X.D.I.I.I.I.I.D.........X
XXX..I.IXIXIX.XXX.XXX.XXX
X.............X.X.X.X.X.X
X.XXXXXXXXXDXXXXX.XXX.XXX
X.X.......X.............X
X.X.XXXXX.XXXXX.XXX.XXX.X
X.......X.....X.X.X.X...X
X.X.XXX.X.XXX.X.X.X.X.XDX
X.X...X.X.X.X.........X.X
X.XXX.X.X.XXXXXXXXXXXXX.X
X.......................X
XXXXXXXXXXXXXXXXXXXXXXXXX

*/

const walls: Array<THREE.Vector3> = [];
const wallGeometry = new THREE.PlaneGeometry(1, 3);
const wallMaterial = new THREE.MeshStandardMaterial({
  color: "#fff",
  side: THREE.DoubleSide,
});
function buildMap() {
  function buildWall(x: number, z: number, rotated?: boolean) {
    const wallMesh = new THREE.Mesh(wallGeometry, wallMaterial);
    const isRotated = rotated ?? false;
    wallMesh.position.set(x, 1.5, z);
    wallMesh.rotation.set(0, isRotated ? 0 : Math.PI / 2, 0);
    scene.add(wallMesh);
    walls.push(wallMesh.position);
  }

  const rows = map.split("\n");
  let x = 1;
  let z = 1;
  for (let rowIndex = 0; rowIndex < rows.length; rowIndex++) {
    const row = rows[rowIndex];
    if (row.length === 1 && row[0] === "") continue;
    for (let colIndex = 0; colIndex < row.length; colIndex++) {
      const col = row[colIndex];
      if (col === "|") {
        let colX =
          colIndex === 0 ? x - 0.5 : colIndex === row.length - 1 ? x + 0.5 : x;
        buildWall(colX, z, false);
        buildWall(colX, z + 0.5, false);
        buildWall(colX, z - 0.5, false);
      } else if (col === "-") {
        let colX = rowIndex > 1 && rowIndex < rows.length - 2 ? x + 0.5 : x;
        buildWall(colX, z, true);
      }
      x += 1;
    }

    z += 1;
    x = 1;
  }
}
buildMap();

// PLAYER
const loader = new THREE.TextureLoader();
function createPlayer(color: string) {
  const playerGeometry = new THREE.SphereBufferGeometry(0.5, 32, 32);
  playerGeometry.clearGroups();
  playerGeometry.addGroup(0, Infinity, 0);
  playerGeometry.addGroup(0, Infinity, 1);

  const colorMaterial = new THREE.MeshBasicMaterial({
    color: color,
  });

  const skin = loader.load("./playerTexture.png");
  skin.flipY = false;
  skin.offset.x = -0.25;

  const skinMaterial = new MeshStandardMaterial({
    map: skin,
    transparent: true,
  });

  const playerMesh = new THREE.Mesh(playerGeometry, [
    skinMaterial,
    colorMaterial,
  ]);
  playerMesh.castShadow = true;
  playerMesh.position.x = 2;
  playerMesh.position.y = 1;
  playerMesh.position.z = 2.5;
  scene.add(playerMesh);

  return playerMesh;
}

// Improvement: Finished bullets can be destroyed and recreated (TEST)
function resetBullet(
  bullet: THREE.Mesh<THREE.SphereGeometry, THREE.Material | THREE.Material[]>
) {
  bullet.position.setY(-5);
}

function createBullet(color: string) {
  const player = createPlayer(color);
  player.geometry.scale(0.25, 0.25, 0.25);
  player.position.setY(-5);
  return player;
}

// COLLISION
// Improvement: https://developer.mozilla.org/en-US/docs/Games/Techniques/3D_collision_detection/Bounding_volume_collision_detection_with_THREE.js
function checkCollision(
  objects: THREE.Vector3[],
  playerMesh: THREE.Mesh<
    THREE.SphereGeometry,
    THREE.Material | THREE.Material[]
  >
) {
  const origin = playerMesh.position.clone();
  for (let index = 0; index < objects.length; index++) {
    const object = objects[index];
    if (origin.distanceToSquared(object) < 0.5) return true;
  }
  return false;
}

// LIGHT
function createEnvironment(): [
  renderer: THREE.WebGLRenderer,
  camera: THREE.PerspectiveCamera
] {
  const ambientLight = new THREE.AmbientLight(undefined, 0.5);
  const directionalLight = new THREE.DirectionalLight("#fff");
  scene.add(ambientLight);
  scene.add(directionalLight);
  // SIZES

  const sizes = {
    width: canvas.clientWidth,
    height: canvas.clientHeight,
  };

  // CAMERA

  const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height);
  scene.add(camera);

  // HELPER

  const axesHelper = new THREE.AxesHelper();
  scene.add(axesHelper);

  // RENDERER

  const renderer = new THREE.WebGLRenderer({ canvas: canvas, antialias: true });

  renderer.setSize(sizes.width, sizes.height);
  renderer.render(scene, camera);

  return [renderer, camera];
}

function runGame(
  env: [renderer: THREE.WebGLRenderer, camera: THREE.PerspectiveCamera],
  thisPlayerName: string,
  onObjectMove: (
    position: THREE.Vector3Tuple,
    rotation: number[],
    type: "smiley" | "bullet"
  ) => void
) {
  const [renderer, camera] = env;
  let shooting: "IDLE" | "TRIGGERED" | "FLYING" | "IMPACT" = "IDLE";
  let input: {
    [key in "ArrowLeft" | "ArrowRight" | "ArrowDown" | "ArrowUp"]: boolean;
  } = {
    ArrowDown: false,
    ArrowRight: false,
    ArrowUp: false,
    ArrowLeft: false,
  };
  window.addEventListener("keydown", (evt) => {
    switch (evt.key) {
      case " ":
        if (shooting === "IDLE") shooting = "TRIGGERED";
      case "ArrowLeft":
      case "ArrowRight":
      case "ArrowUp":
      case "ArrowDown":
        input = { ...input, [evt.key]: true };
      default:
        break;
    }
  });

  window.addEventListener("keyup", (evt) => {
    switch (evt.key) {
      case "ArrowLeft":
      case "ArrowRight":
      case "ArrowUp":
      case "ArrowDown":
        input = { ...input, [evt.key]: false };
      default:
        break;
    }
  });

  // ANIMATION
  const clock = new THREE.Clock();
  const moveSpeed = 5;
  const bulletSpeed = 7;
  const turnSpeed = 3.5;
  let player = players.get(thisPlayerName)!;
  function tick() {
    requestAnimationFrame(tick);
    const delta = clock.getDelta();

    const oldPosition = player.smiley.position.clone();
    const oldRotation = player.smiley.rotation.clone();
    if (input.ArrowDown) {
      player.smiley.translateZ(moveSpeed * delta);
    }
    if (input.ArrowUp) {
      player.smiley.translateZ(-moveSpeed * delta);
    }
    if (input.ArrowLeft) {
      player.smiley.rotation.y += turnSpeed * delta;
    }
    if (input.ArrowRight) {
      player.smiley.rotation.y -= turnSpeed * delta;
    }

    if (shooting === "TRIGGERED") {
      player.bullet.rotation.setFromVector3(player.smiley.rotation.toVector3());
      player.bullet.position.setFromMatrixPosition(player.smiley.matrix);
      shooting = "FLYING";
    } else if (shooting === "FLYING") {
      player.bullet.translateZ(-bulletSpeed * delta);

      if (checkCollision(walls, player.bullet)) {
        resetBullet(player.bullet);
        shooting = "IDLE";
      }
      // Improvement: Only send the intial position and direction and let it fly on the client receiving the information.
      // On Hit should be sent by the "firing" client
      onObjectMove(
        player.bullet.position.toArray(),
        player.bullet.rotation.toArray(),
        "bullet"
      );
    }

    if (checkCollision(walls, player.smiley)) {
      player.smiley.position.set(oldPosition.x, oldPosition.y, oldPosition.z);
    } else {
      if (
        oldPosition.distanceTo(player.smiley.position) !== 0 ||
        oldRotation.y !== player.smiley.rotation.y
      ) {
        onObjectMove(
          player.smiley.position.toArray(),
          player.smiley.rotation.toArray(),
          "smiley"
        );
      }
      camera.position.set(
        player.smiley.position.x,
        player.smiley.position.y,
        player.smiley.position.z
      );
      camera.rotation.set(
        player.smiley.rotation.x,
        player.smiley.rotation.y,
        player.smiley.rotation.z
      );
    }

    while (updates.length > 0) {
      const work = updates.pop()!;
      if (work.userName !== thisPlayerName) {
        let player = players.get(work.userName)!;

        const target = work.type === "smiley" ? player.smiley : player.bullet;
        target.position.set(...work.position);
        target.rotation.set(
          work.rotation[0],
          work.rotation[1],
          work.rotation[2]
        );
      }
    }

    renderer.render(scene, camera);
  }
  tick();
}

// USER JOIN
const form = document.getElementById("form")!;

form.addEventListener("submit", (evt) => {
  evt.preventDefault();
  evt.stopPropagation();

  const formData = new FormData(evt.target as HTMLFormElement);
  const userName = formData.get("name")! as string;
  const color = formData.get("color")! as string;

  const env = createEnvironment();
  const player = createPlayer(color);
  const bullet = createBullet(color);
  players.set(userName, {
    smiley: player,
    bullet: bullet,
    userName,
  });

  runGame(env, userName, (position, rotation, type) => {
    socket.emit("objectMoved", { userName, position, rotation, type });
  });

  socket.on("objectMoved", (event: ObjectMoved) => {
    updates.push(event);
  });

  socket.on("playerlistUpdated", (networkPlayers: PlayerlistUpdated) => {
    console.log(
      `player ${userName} received players: ${JSON.stringify(networkPlayers)}`
    );
    for (const player of networkPlayers) {
      if (!players.has(player.userName)) {
        const newPlayer = createPlayer(player.color);
        const newPlayerBullet = createBullet(player.color);
        players.set(player.userName, {
          smiley: newPlayer,
          userName: player.userName,
          bullet: newPlayerBullet,
        });
      }
    }
    for (const existingPlayer of [...players.keys()]) {
      if (!networkPlayers.some((s) => s.userName === existingPlayer)) {
        const player = players.get(existingPlayer)!;
        scene.remove(player.smiley);
        players.delete(existingPlayer);
      }
    }
  });
  socket.emit("join", { userName, color });
});
